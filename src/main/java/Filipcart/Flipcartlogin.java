package Filipcart;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
public class Flipcartlogin {

	@Test()
		//public static void main(String[] args) throws Exception {
		
	public void flifcart()
	{
		
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		
		//object Creation
		ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.switchTo().alert().dismiss();
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
			
//		3) Mouse over on Electronics
	//	driver.findElementByXPath("//span[contains(text(),'Electronics')]").click();
		
		  Actions action = new Actions(driver);
	       WebElement componentObject=driver.findElementByXPath("//span[contains(text(),'Electronics')]");
	     action.moveToElement(componentObject).perform();
		
						
//		4) Click on any specific mobile Model (Only Mi/RealMe)
		
		driver.findElementByXPath("//a[(text()='Realme')]").click();
		
				
		
		
				
//		5) Verify Page title should contains (Mi/RealMe) Mobile Phones 
		
			if(driver.getTitle().contains("Mi Mobiles")) {
					System.out.println("Mi Mobiles title verified");
				}
				else {
					System.out.println("Title mismatch");
				}
// 6) Click on Newest First - select the first item
			
				driver.findElementByXPath("//div[text()='Newest First']").click();

//Thread.sleep(5000);
				
// 7) Print all the Product Name along with the price
List<WebElement> allMobiles = driver.findElementsByClassName("_3wU53n");
for (WebElement eachMobile : allMobiles) {
	System.out.println(eachMobile.getText());
}
List<WebElement> allprices = driver.findElementsByClassName("_1uv9Cb");
for (WebElement eachprice : allprices) {
	System.out.println(eachprice.getText());
}
 
//8) Click on the first product
				//driver.findElementByXPath("//div[text()='Realme C1 (Navy Blue, 16 GB)']");
	
			driver.findElementByXPath("//*[@class=\"_3wU53n\"][1]").click();	
				
		
			driver.findElementByXPath("//div[text()='Newest First']").click();
			driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
			Set<String> allWindows = driver.getWindowHandles();
			List<String> lst = new ArrayList<> ();
			lst.addAll(allWindows);
			driver.switchTo().window(lst.get(1));
			
			System.out.println("The title is:"+driver.getTitle());
			// Verify the title of the new window
					if(driver.getTitle().contains("mobileModelName")) {
						System.out.println("Title Matches");
					}else {
						System.out.println(driver.getTitle());
						System.out.println("Title does not match");
					}
					// Get the ratings and reviews
					String ratings = driver.findElementByXPath("//span[contains(text(),'Ratings')]").getText();
					System.out.println(ratings);
					String reviews = driver.findElementByXPath("//span[contains(text(),'Reviews')]").getText();
					System.out.println(reviews);
					
					// Close the browser
					driver.quit();

				
			
			
			
				
				
}
}



/*  old code
 * 
 * package Filipcart;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
public class Flipcartlogin {

	@Test
	
	public static void main(String[] args) throws Exception {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		
		//object Creation
		ChromeDriver driver = new ChromeDriver();
			driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.switchTo().alert().dismiss();
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
			
//		3) Mouse over on Electronics
	//	driver.findElementByXPath("//span[contains(text(),'Electronics')]").click();
		
		  Actions action = new Actions(driver);
	       WebElement componentObject=driver.findElementByXPath("//span[contains(text(),'Electronics')]");
	     action.moveToElement(componentObject).perform();
		
						
//		4) Click on any specific mobile Model (Only Mi/RealMe)
		
		driver.findElementByXPath("//a[(text()='Realme')]").click();
		
				
		
		
				
//		5) Verify Page title should contains (Mi/RealMe) Mobile Phones 
		
			if(driver.getTitle().contains("Mi Mobiles")) {
					System.out.println("Mi Mobiles title verified");
				}
				else {
					System.out.println("Title mismatch");
				}
// 6) Click on Newest First - select the first item
			
				driver.findElementByXPath("//div[text()='Newest First']").click();

Thread.sleep(5000);
				
// 7) Print all the Product Name along with the price
List<WebElement> allMobiles = driver.findElementsByClassName("_3wU53n");
for (WebElement eachMobile : allMobiles) {
	System.out.println(eachMobile.getText());
}
List<WebElement> allprices = driver.findElementsByClassName("_1uv9Cb");
for (WebElement eachprice : allprices) {
	System.out.println(eachprice.getText());
}
 
//8) Click on the first product
				//driver.findElementByXPath("//div[text()='Realme C1 (Navy Blue, 16 GB)']");
	
			driver.findElementByXPath("//*[@class=\"_3wU53n\"][1]").click();	
				
		
			driver.findElementByXPath("//div[text()='Newest First']").click();
			driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
			Set<String> allWindows = driver.getWindowHandles();
			List<String> lst = new ArrayList<> ();
			lst.addAll(allWindows);
			driver.switchTo().window(lst.get(1));
			
			System.out.println("The title is:"+driver.getTitle());
			// Verify the title of the new window
					if(driver.getTitle().contains("mobileModelName")) {
						System.out.println("Title Matches");
					}else {
						System.out.println(driver.getTitle());
						System.out.println("Title does not match");
					}
					// Get the ratings and reviews
					String ratings = driver.findElementByXPath("//span[contains(text(),'Ratings')]").getText();
					System.out.println(ratings);
					String reviews = driver.findElementByXPath("//span[contains(text(),'Reviews')]").getText();
					System.out.println(reviews);
					
					// Close the browser
					driver.quit();

				//driver.close();
				
		
			
			
			
			
			
			
				
				
				 
//driver.close();   }
}

*/
 

